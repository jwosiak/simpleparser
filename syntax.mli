module Syntax :
  sig
    val left_assoc_ops : string list
    val right_assoc_ops : string list
    val whiles : string list
    val ifs : string list
    val elses : string list
    val separators : string list
    val lparentheses : string list
    val rparentheses : string list
    val lbraces : string list
    val rbraces : string list
    val commas : string list
    val returns : string list
    val prints : string list
    val printlns : string list
    val reads : string list
    val functions : string list
    val op_prior : string -> int
  end
