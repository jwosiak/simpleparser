open Syntax
open Lexer       

module Parser =
  struct
    type arith_exp =
      | Value of string
      | Sum of arith_exp * arith_exp
      | Prod of arith_exp * arith_exp
      | Expon of arith_exp * arith_exp
      | Assign of arith_exp * arith_exp
      | Div of arith_exp * arith_exp
      | Mod of arith_exp * arith_exp
      | Sub of arith_exp * arith_exp
      | LT of arith_exp * arith_exp
      | GT of arith_exp * arith_exp
      | EQ of arith_exp * arith_exp
      | Fun of string * arith_exp list
      | And of arith_exp * arith_exp
      | Or of arith_exp * arith_exp
      | Neg_logical of arith_exp
      | Neg_sign of arith_exp
      | Cast_to_float of arith_exp
      | Cast_to_int of arith_exp
      | Array_index of arith_exp * arith_exp

    type expr =
      | Empty_expr
      | Return of arith_exp
      | Print of arith_exp
      | Println of arith_exp
      | Read of arith_exp
      | Arith_action of arith_exp
      | Sequence of expr * expr
      | Block of expr
      | If of arith_exp * expr
      | While of arith_exp * expr
      | If_else of arith_exp * expr * expr
      | Fun_defin of string * string list * expr


    module Environment =
      struct
	type funct = { argsNum : int; body : expr; argsNames : string list}
	type dataType =
	  | Null
	  | Int of int
	  | Float of float
	  | String of string
		       
	module EnvironmentMap = Map.Make(String)

	let globalFunctions =
	  ref EnvironmentMap.(
	    add "noop" { argsNum = 0; body = Empty_expr; argsNames = [] } empty)
      end
					

    let rpn input =
      let rec take_from_stack pred stack = match stack with
	  [] -> failwith "Parser.rpn.take_from_stack: reached end of stream"
	| x::xs -> if pred x then ([], stack)
		   else
		     let p = take_from_stack pred xs
		     in (x::fst p, snd p)
      and lower_operators op stack = match stack with
	  (Lexer.Left_assoc_op x as op2)::xs ->
	  if Syntax.op_prior x < Syntax.op_prior op then ([], stack)
	  else let p = lower_operators op xs
	       in (op2::fst p, snd p)
	| (Lexer.Right_assoc_op x as op2)::xs ->
	   if Syntax.op_prior x <= Syntax.op_prior op then ([], stack)
	   else let p = lower_operators op xs
		in (op2::fst p, snd p)
	| (Lexer.Unary_op x as op2)::xs ->
	   let p = lower_operators op xs
	   in (op2::fst p, snd p)
	| _ -> ([], stack)
      in
      let rec rpn_aux input stack unaryOpFlag = match (input, unaryOpFlag) with
	  ([], _) -> stack
	| ((Lexer.Value str)::(Lexer.Lparenthesis::xs as ys), _) ->
	   rpn_aux ys ((Lexer.Value str)::stack) false
	| ((Lexer.Value str as v)::xs, _) -> v::rpn_aux xs stack false
	| (Lexer.Comma::xs, _) ->
	   let fun_params = take_from_stack (fun x-> x = Lexer.Lparenthesis) stack
	   in (fst fun_params) @ (rpn_aux xs (snd fun_params) true)
	| ((Lexer.Left_assoc_op x as oper)::xs, false) ->
	   let p = lower_operators x stack
	   in fst p @ (rpn_aux xs (oper::snd p) true)
	| ((Lexer.Left_assoc_op x)::xs, true) ->
	   rpn_aux xs ((Lexer.Unary_op x)::stack) true
	| ((Lexer.Right_assoc_op x as oper)::xs, _) ->
	   let p = lower_operators x stack
	   in fst p @ (rpn_aux xs (oper::snd p) true)
	| (Lexer.Lparenthesis::xs, _) -> rpn_aux xs (Lexer.Lparenthesis::stack) true
	| (Lexer.Rparenthesis::xs, _) ->
	   let p = take_from_stack (fun x -> x = Lexer.Lparenthesis) stack
	   in let p2 = match snd p with
		  Lexer.Lparenthesis::(Lexer.Value x as f)::ys -> ([f], ys)
		| Lexer.Lparenthesis::ys -> ([], ys)
		| _ -> failwith "Parser.rpn: wrong arrangement of parentheses"
	      in fst p2 @ (fst p) @ (rpn_aux xs (snd p2) false)
	| _ -> failwith "Parser.rpn: forbidden chars inside arithmetic expression"
      in rpn_aux input [] true
  


    let arithTokensToAST arg1 arg2 =
      [(Lexer.Left_assoc_op "+", Sum(arg1, arg2));
       (Lexer.Left_assoc_op "-", Sub(arg1, arg2));
       (Lexer.Left_assoc_op "*", Prod(arg1, arg2));
       (Lexer.Left_assoc_op "/", Div(arg1, arg2));
       (Lexer.Left_assoc_op "%", Mod(arg1, arg2));
       (Lexer.Right_assoc_op "^", Expon(arg1, arg2));
       (Lexer.Right_assoc_op "=", Assign(arg1, arg2));
       (Lexer.Left_assoc_op "<", LT(arg1, arg2));
       (Lexer.Left_assoc_op ">", GT(arg1, arg2));
       (Lexer.Left_assoc_op "==", EQ(arg1, arg2));
       (Lexer.Left_assoc_op "&&", And(arg1, arg2));
       (Lexer.Left_assoc_op "||", Or(arg1, arg2));
       (Lexer.Right_assoc_op "<-", Array_index(arg1, arg2));
       (Lexer.Unary_op "-", Neg_sign arg1);
       (Lexer.Unary_op "*", Cast_to_float arg1);
       (Lexer.Unary_op "**", Cast_to_int arg1);
       (Lexer.Unary_op "!", Neg_logical arg1);]



    let rec eval_rpn xl stack =
      let rec matchTokenToExpr token ys = match ys with
	  [] -> failwith "Parser.eval_rpn: undefined operator!"
	| x::xs -> if fst x = token then snd x else matchTokenToExpr token xs
      and take n ys = match (n, ys) with
	  (0, xs) -> ([], xs)
	| (_, []) -> ([], [])
	| (i, x::xs) -> let nextTake = take (i-1) xs
			in (x::(fst nextTake), snd nextTake)
      in
      match xl with  
	[] -> List.hd stack
      | x::xs -> match x with
		   Lexer.Value str ->
		   Environment.(
		     EnvironmentMap.(
		       if mem str !globalFunctions then
			 let funct = find str !globalFunctions
			 in let takenStack = take funct.argsNum stack
			    in eval_rpn xs(Fun(str, List.rev @@ fst takenStack)::
					     (snd takenStack) )
		       else
			 eval_rpn xs (Value str::stack)
		     )
		   )
		 | Lexer.Unary_op str as oper ->
		    let hd1 = List.hd stack
		    and nextStack = List.tl stack
		    in let topValue = matchTokenToExpr oper (arithTokensToAST hd1 hd1)
		       in eval_rpn xs (topValue::nextStack)
		    
		 | oper -> let hd1 = List.hd stack 
			   and hd2 = List.hd (List.tl stack)
			   and nextStack = List.tl (List.tl stack)
			   in
			   let topValue =
			     matchTokenToExpr oper (arithTokensToAST hd2 hd1)
			   in eval_rpn xs (topValue::nextStack)
				       
    let arithToAst xs = eval_rpn (rpn xs) []

				       
    type 'a nestedList =
      | Nil
      | NN of 'a nestedList * 'a nestedList
      | N of 'a * 'a nestedList
		     

				       
    let ast tokens =
      let rec groupedBlocks ys =
	let rec read_block ltokens opened_braces = match (ltokens, opened_braces) with
	    ([], _) -> failwith "Parser.read_block: reached end of stream (are you missing '}' ?)"
	  | (Lexer.Rbrace::xs, 1) -> ([], xs)
	  | (x::xs, n) ->
	     let p = match x with
		 Lexer.Lbrace -> read_block xs @@ n+1
	       | Lexer.Rbrace -> read_block xs @@ n-1
	       | _ -> read_block xs n
	     in (x::fst p, snd p)
	and read_line ys = match ys with
	    [] -> failwith "Parser.read_line: reached end of stream (are you missing ';' ?)"
	  | Lexer.Separator::xs -> ([], xs)
	  | x::xs -> let p = read_line xs in (x::fst p, snd p)
	in match ys with     
	     [] -> Nil
	   | Lexer.Separator::xs -> groupedBlocks xs
	   | Lexer.Lbrace::xs ->
	      let block = read_block xs 1
	      in NN (groupedBlocks @@ fst block, groupedBlocks @@ snd block)
	   | xs -> let line = read_line xs
		   in N (fst line, groupedBlocks @@ snd line)
      and readArgsList ys = 
	let rec readArgs zs =
	  match zs with
	    Lexer.Comma::xs -> readArgs xs
	  | Lexer.Rparenthesis::xs -> []
	  | (Lexer.Value str)::xs -> str::readArgs xs
	  | _ -> failwith "Parser.readArgs: function's parameters list error"
	in
	match ys with
	  Lexer.Lparenthesis::xs -> readArgs xs
	| _ -> failwith "Parser.readArgs: function's parameters list error"
      in
      let rec astIter ys = match ys with
	| Nil -> Empty_expr
	| NN (block, Nil) -> astIter block
	| N(Lexer.If_instr::cond, NN(block, N([Lexer.Else_instr],
					      NN(elseBlock,xs) ) ) ) ->
	   Sequence (If_else (arithToAst cond, Block(astIter block),
			      Block(astIter elseBlock) ), astIter xs)
	| N(Lexer.If_instr::cond, NN(block, xs)) ->
	   Sequence (If (arithToAst cond, Block(astIter block)), astIter xs)
	| N(Lexer.While_instr::cond, NN(block, xs)) ->
	   Sequence (While (arithToAst cond, Block(astIter block)), astIter xs)
	| N(Lexer.While_instr::cond, N(line, xs)) ->
	   Sequence (While (arithToAst cond, astIter (N(line, Nil)) ), astIter xs)
	| N(Lexer.Return_instr::result, xs) ->
	   Sequence (Return (arithToAst result), astIter xs)
	| N(Lexer.Function_instr::(Lexer.Value name)::args, NN(body, xs)) ->
	   let _ =
	     Environment.(
	       EnvironmentMap.(
		 globalFunctions :=
		   add name { argsNum = List.length @@ readArgsList args; argsNames = [];
			      body = Empty_expr } !globalFunctions
	       )
	     )
	   in
	   Sequence (Fun_defin(name, readArgsList args, astIter body), astIter xs)
	| N(Lexer.Print_instr::result, xs) ->
	   Sequence (Print (arithToAst result), astIter xs)
	| N(Lexer.Println_instr::result, xs) ->
	   Sequence (Println (arithToAst result), astIter xs)
	| N([Lexer.Read_instr; Lexer.Value name], xs) ->
	   Sequence (Read (Value name), astIter xs)
	| N ( ( (Lexer.Value _)::zs as xs), rs) ->
	   Sequence (Arith_action(arithToAst xs), astIter rs)
	| N ( ( Lexer.Lparenthesis::zs as xs), rs) ->
	   Sequence (Arith_action(arithToAst xs), astIter rs)
	| N (line, Nil) -> Arith_action (arithToAst line)
	| _ -> failwith "Parser.ast: undefined expression"
      in astIter @@ groupedBlocks tokens
				   
  end
;;
