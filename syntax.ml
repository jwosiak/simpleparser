module Syntax =
  struct	   
    let left_assoc_ops =
      ["+"; "-"; "*"; "/"; "%"; "<"; ">"; "=="; "&&"; "||"; "**"; "!"]
    let right_assoc_ops = ["="; "^"; "<-"]
    let whiles = ["while"]
    let ifs = ["if"]
    let elses = ["else"]
    let separators = [";"]
    let lparentheses = ["("]
    let rparentheses = [")"]
    let lbraces = ["{"]
    let rbraces = ["}"]
    let commas = [","]
    let returns = ["return"]
    let prints = ["print"]
    let printlns = ["println"]
    let reads = ["read"]
    let functions = ["function"; "fun"]

    let op_prior operator = match operator with
	"=" -> 0
      | "<" -> 1
      | ">" -> 1
      | "==" -> 1
      | "||" -> 2
      | "&&" -> 3
      | "+" -> 5
      | "-" -> 5
      | "*" -> 7
      | "/" -> 7
      | "%" -> 7
      | "<-" -> 8
      | "^" -> 8
      | _ -> 1
  end
;;
