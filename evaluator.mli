open Parser

module Evaluator :
sig
  val typeFailure : 'a -> 'b
  val string_of_dataType : Parser.Environment.dataType -> string
  val evalAst :
    Parser.expr ->
    Parser.Environment.dataType Parser.Environment.EnvironmentMap.t ->
    Parser.Environment.dataType *
      Parser.Environment.dataType Parser.Environment.EnvironmentMap.t
end

