open Parser
open Evaluator
open Lexer

let main =
  let read_file fname =
    let input_channel = open_in fname
    in
    let rec read_single_iter u =
      let str =
	try input_line input_channel with
	  _ -> let _ = close_in input_channel
	       in ""
      in if str = "" then [] else str::(read_single_iter ())
    in
    String.concat " " @@ read_single_iter ()
  and globalVariables = ref Parser.Environment.(EnvironmentMap.(add "" Null empty))
  and command = ref ""
  in
  let pair = ref (Parser.Environment.Null, !globalVariables)
  in
  begin
    while !command <> ":quit" do
      command := read_line();
      if !command <> "" then
	begin
	  if !command <> ":quit" && (!command).[0] = ':' then
	    (try 
		command := read_file @@ String.sub !command 1 (String.length !command - 1)
	      with
		_ -> print_endline ("Failed to open file: '" ^ !command ^ "'"));

	  try
	    begin
	      pair :=
		Evaluator.evalAst (Parser.ast @@ Lexer.tokens !command) !globalVariables;
	      print_endline @@ "Value = " ^ Evaluator.string_of_dataType (fst !pair);
	      globalVariables := snd !pair;
	    end
	  with
	    Failure exc -> print_endline exc
	  | _ -> print_endline "Unexpected error occurred";


	end
    done;


  end
;;
  

  
