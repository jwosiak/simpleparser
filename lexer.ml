open Syntax

module Lexer = 
  struct
    type token =
      | Value of string
      | Left_assoc_op of string
      | Right_assoc_op of string
      | Unary_op of string
      | While_instr
      | If_instr
      | Else_instr
      | Return_instr
      | Print_instr
      | Println_instr
      | Read_instr
      | Function_instr
      | Lparenthesis
      | Rparenthesis
      | Lbrace
      | Rbrace
      | Separator
      | Comma

    
    let str_tok_equivalent = Syntax.(
	[(left_assoc_ops, fun str -> Left_assoc_op str);
	 (right_assoc_ops, fun str -> Right_assoc_op str);
	 (lparentheses, fun _ -> Lparenthesis); (rparentheses, fun _ -> Rparenthesis);
	 (lbraces, fun _ -> Lbrace); (rbraces, fun _ -> Rbrace);
	 (separators, fun _ -> Separator ); (commas, fun _ -> Comma);
	 (whiles, fun _ -> While_instr); (ifs, fun _ -> If_instr);
	 (elses, fun _ -> Else_instr); (returns, fun _ -> Return_instr);
	 (prints, fun _ -> Print_instr); (functions, fun _ -> Function_instr);
	 (printlns, fun _ -> Println_instr); (reads, fun _ -> Read_instr)] )


    let moreSpaceAfter string char =
      let rec split i n xs =
	if i < n then
	  if string.[i] = char then
	    split (i+1) n (""::xs)
	  else
	    let head = (List.hd xs) ^ (String.make 1 string.[i] )
	    in split (i+1) n (head::(List.tl xs))
	else xs
      in
      String.concat (" " ^ String.make 1 char ^ " ")
		    (List.rev @@ split 0 (String.length string) [""])
		    
    let tokens string =
      let str = List.fold_left moreSpaceAfter string [';'; '('; ')'; '{'; '}']
      in
      let slen = String.length str
      and is_white_space c = List.mem c [' '; '\t'; '\n'; '\r']
      and is_part_of_string c = List.fold_left 
                                  (fun b (l,r) -> b || (l <= c && c <= r))
                                  false [('A', 'Z');('a', 'z');('0', '9');('.','.');
					 ('"','"')]
      in 
      let rec string_parts from len is_string =
	let curr_str = String.sub str from len
	in
        if from + len = slen then
	  [curr_str]
        else
          if is_white_space str.[from + len] then
            match curr_str with
              "" -> string_parts (from + len + 1) 0 false
            | s  -> s::string_parts (from + len + 1) 0 false
          else
	    match (is_string, is_part_of_string str.[from + len], len) with
	      (false, false, leng) -> string_parts from (leng+1) false
	    | (true, true, leng) -> string_parts from (leng+1) true
	    | (false, true, 0) -> string_parts from 1 true
	    | _ -> curr_str::string_parts (from + len) 0 false
      in
      let raw_tokens = List.filter (fun s -> s <> "") @@ string_parts 0 0 false
      in
      let rec string_to_token xl str = match xl with
	  [] -> Value str
	| x::xs -> if List.mem str (fst x) then (snd x) str
		   else string_to_token xs str
      in List.map (string_to_token  str_tok_equivalent) raw_tokens

  end
;;

  
