ocamlc -c syntax.mli
ocamlc -c syntax.ml

ocamlc -c lexer.mli
ocamlc -c lexer.ml

ocamlc -c parser.mli
ocamlc -c parser.ml

ocamlc -c evaluator.mli
ocamlc -c evaluator.ml

ocamlc -c main.ml

ocamlc -o Imperor syntax.cmo lexer.cmo parser.cmo evaluator.cmo main.cmo
ocamlrun Imperor

rm *.cmo
rm *.cmi