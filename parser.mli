open Lexer

module Parser :
sig
  type arith_exp =
    Value of string
    | Sum of arith_exp * arith_exp
    | Prod of arith_exp * arith_exp
    | Expon of arith_exp * arith_exp
    | Assign of arith_exp * arith_exp
    | Div of arith_exp * arith_exp
    | Mod of arith_exp * arith_exp
    | Sub of arith_exp * arith_exp
    | LT of arith_exp * arith_exp
    | GT of arith_exp * arith_exp
    | EQ of arith_exp * arith_exp
    | Fun of string * arith_exp list
    | And of arith_exp * arith_exp
    | Or of arith_exp * arith_exp
    | Neg_logical of arith_exp
    | Neg_sign of arith_exp
    | Cast_to_float of arith_exp
    | Cast_to_int of arith_exp
    | Array_index of arith_exp * arith_exp
  type expr =
    Empty_expr
    | Return of arith_exp
    | Print of arith_exp
    | Println of arith_exp
    | Read of arith_exp
    | Arith_action of arith_exp
    | Sequence of expr * expr
    | Block of expr
    | If of arith_exp * expr
    | While of arith_exp * expr
    | If_else of arith_exp * expr * expr
    | Fun_defin of string * string list * expr
  module Environment :
  sig
    type funct = { argsNum : int; body : expr; argsNames : string list; }
    type dataType = Null | Int of int | Float of float | String of string
    module EnvironmentMap :
    sig
      type key = String.t
      type 'a t = 'a Map.Make(String).t
      val empty : 'a t
      val is_empty : 'a t -> bool
      val mem : key -> 'a t -> bool
      val add : key -> 'a -> 'a t -> 'a t
      val singleton : key -> 'a -> 'a t
      val remove : key -> 'a t -> 'a t
      val merge :
        (key -> 'a option -> 'b option -> 'c option) ->
        'a t -> 'b t -> 'c t
      val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
      val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
      val iter : (key -> 'a -> unit) -> 'a t -> unit
      val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
      val for_all : (key -> 'a -> bool) -> 'a t -> bool
      val exists : (key -> 'a -> bool) -> 'a t -> bool
      val filter : (key -> 'a -> bool) -> 'a t -> 'a t
      val partition : (key -> 'a -> bool) -> 'a t -> 'a t * 'a t
      val cardinal : 'a t -> int
      val bindings : 'a t -> (key * 'a) list
      val min_binding : 'a t -> key * 'a
      val max_binding : 'a t -> key * 'a
      val choose : 'a t -> key * 'a
      val split : key -> 'a t -> 'a t * 'a option * 'a t
      val find : key -> 'a t -> 'a
      val map : ('a -> 'b) -> 'a t -> 'b t
      val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
    end
    val globalFunctions : funct EnvironmentMap.t ref
  end
  val rpn : Lexer.token list -> Lexer.token list
  val arithTokensToAST :
    arith_exp -> arith_exp -> (Lexer.token * arith_exp) list
  val eval_rpn : Lexer.token list -> arith_exp list -> arith_exp
  val arithToAst : Lexer.token list -> arith_exp
  type 'a nestedList =
    Nil
    | NN of 'a nestedList * 'a nestedList
    | N of 'a * 'a nestedList
  val ast : Lexer.token list -> expr
end

