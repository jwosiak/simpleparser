module Lexer :
sig
  type token =
    Value of string
    | Left_assoc_op of string
    | Right_assoc_op of string
    | Unary_op of string
    | While_instr
    | If_instr
    | Else_instr
    | Return_instr
    | Print_instr
    | Println_instr
    | Read_instr
    | Function_instr
    | Lparenthesis
    | Rparenthesis
    | Lbrace
    | Rbrace
    | Separator
    | Comma
  val str_tok_equivalent : (string list * (string -> token)) list
  val moreSpaceAfter : string -> char -> string
  val tokens : string -> token list
end
