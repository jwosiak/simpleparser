open Parser

module Evaluator =
  struct    			
    let typeFailure _ = failwith "Not availible for this type!"

    let string_of_dataType value =
      Parser.(
	Environment.(
	  match value with
	    Int x -> string_of_int x
	  | Float x -> string_of_float x
	  | String x -> "\"" ^ x ^ "\""
	  | Null -> "Null"
	))

    let rec evalAst expression variables =
      Parser.(
	Environment.(
	  let rec updateVariables dict1 dict2 =
	    EnvironmentMap.(
	      filter (fun key elem -> (mem key dict1) ||
					((String.length key > 0) && key.[0] = '_')) dict2
	    )
	  and allGlobals dict =
	    EnvironmentMap.(
	      fold (fun key elem value -> if (String.length key > 0) && key.[0] = '_' then
					    (key, elem)::value
					  else value) dict []
	    )
	  and bool_of_value num = match num with
	      Int 0 -> false
	    | Float 0.0 -> false
	    | _ -> true
	  in
	  let rec evalArith expression vars =
	    let (>>) f g x y = g(f x y)
	    and evalBoolean arithExpr vars =
	      let pair = evalArith arithExpr vars
	      in (bool_of_value @@ fst pair, snd pair)
	    and int_of_bool b = if b then 1 else 0
	    and float_of_bool b = if b then 1.0 else 0.0
	    and stringToData str =
	      if (str.[0] = '"' && str.[String.length str - 1] = '"') then
		String (String.sub str 1 (String.length str - 2))
	      else
		try Int (int_of_string str) with
		  _ -> try Float (float_of_string str) with
			 _ ->
			 if EnvironmentMap.mem str vars then
			   EnvironmentMap.find str vars
			 else
			   failwith @@ "Undefined variable: " ^ str
	    and castTypes num = match num with
		Int x -> Float (float_of_int x)
	      | Float x -> Int (int_of_float x)
	      | _ -> failwith "Evaluator.castTypes: not possible situation"
	    in
	    let evalRec expr1 expr2 funInt funFloat =
	      let applyToFun pair1 pair2 =
		match (fst pair1, fst pair2) with
		  (Int x, Int y) -> Int (funInt x y)
		| (Float x, Float y) -> Float (funFloat x y)
		| _ -> failwith "Evaluator.applyToFun: undefined operation for these types"
	      in
	      let pair1 = evalArith expr1 vars
	      in let pair2 = evalArith expr2 (updateVariables vars @@ snd pair1)
		 in (applyToFun pair1 pair2, updateVariables vars @@ snd pair2)
	    and hashArrayElement arrayID elem =
	      "_" ^(string_of_dataType arrayID) ^ "asdfghjkl+lkjhgfdsa-qwertyuiop" ^
		(string_of_dataType elem)
	    in
	    match expression with
	      Value str -> (stringToData str, vars)
	    | Neg_sign x -> evalRec x x (fun a _ -> 0 - a) (fun a _ -> 0.0 -. a)
	    | Cast_to_float x -> let pair = evalArith x vars
				 in (castTypes @@ fst pair, snd pair)
	    | Cast_to_int x -> let pair = evalArith x vars
			       in (castTypes @@ fst pair, snd pair)
	    | Neg_logical x -> evalRec x x (fun a _ -> if a = 0 then 1 else 0)
				       (fun a _ -> if a = 0.0 then 1.0 else 0.0)
	    | And (x, y) -> let pair1 = evalBoolean x vars
			    in if not @@ fst pair1 then
				 (Int 0, snd pair1)
			       else
				 let pair2 = evalBoolean y @@ snd pair1
				 in			       
				 (Int(int_of_bool ((fst pair1) && (fst pair2)) ), snd pair2 )
	    | Or (x, y) -> let pair1 = evalBoolean x vars
			   in if fst pair1 then
				(Int 1, snd pair1)
			      else
				let pair2 = evalBoolean y @@ snd pair1
				in
				(Int (int_of_bool ((fst pair1) || (fst pair2))), snd pair2 )
	    | Sum(x, y) -> evalRec x y (+) (+.)
	    | Sub(x, y) -> evalRec x y (-) (-.)
	    | Prod(x, y) -> evalRec x y ( * ) ( *.)
	    | Div(x, y) -> evalRec x y (/) (/.)
	    | Mod(x, y) -> evalRec x y (mod) (typeFailure)
	    | Assign(Value id, y) ->
	       let pair = evalArith y vars
	       in (fst pair, EnvironmentMap.add id (fst pair) (snd pair))
	    | Expon(x, y) ->
	       evalRec x y
		       (fun a b -> int_of_float @@ (float_of_int a) ** (float_of_int b) )
		       ( ** )
	    | Assign(Array_index (x, y), z) ->
	       let pair1 = evalArith x variables
	       in let pair2 = evalArith y (snd pair1)
		  in let name = hashArrayElement (fst pair1) (fst pair2)
		     in evalArith (Assign(Value name, z))
				  (EnvironmentMap.add name (fst pair2) (snd pair2))
	    | Array_index (x, y) ->
	       let pair1 = evalArith x variables
	       in let pair2 = evalArith y (snd pair1)
		  in let name = hashArrayElement (fst pair1) (fst pair2)
		     in
		     if EnvironmentMap.mem name (snd pair2) then
		       (EnvironmentMap.find name (snd pair2), snd pair2)
		     else (Null, snd pair2)
	    | LT(x, y) -> evalRec x y ( (<) >> int_of_bool) ( (<) >> float_of_bool)
	    | GT(x, y) -> evalRec x y ( (>) >> int_of_bool) ( (>) >> float_of_bool) 
	    | EQ(x, y) -> evalRec x y ( (=) >> int_of_bool) ( (=) >> float_of_bool)
	    | Fun(name, xs) ->
	       let rec evalParams ys variables =
		 match ys with
		   [] -> []
		 | z::zs -> let pair = evalArith z variables
			    in (fst pair)::(evalParams zs (snd pair) )
	       and zipWith xs ys = match (xs, ys) with
		   (z::zs, w::ws) -> (z, w)::(zipWith zs ws)
		 | _ -> []
	       in
	       let ys = evalParams xs vars
	       and f = EnvironmentMap.find name !globalFunctions
	       in let funVariables1 =
		    List.fold_left (fun dict (id, value) ->
				    EnvironmentMap.add id value dict)
				   EnvironmentMap.empty
				   (zipWith f.argsNames ys)
		  and funVariables2 =
		    List.fold_left (fun dict (id, value) ->
				    EnvironmentMap.add id value dict)
				   EnvironmentMap.empty
				   ((zipWith f.argsNames ys) @ allGlobals vars)
		  in let pair = evalAst f.body (updateVariables funVariables1 funVariables2)
		     in (fst pair, List.fold_left (fun dict (id, value) ->
						   EnvironmentMap.add id value dict)
						  vars (allGlobals @@ snd pair))
	    | _ -> failwith "Evaluator.evalArith: unknown arithmetic expression"
	  in
	  match expression with
	    Empty_expr -> (Null, variables)
	  | Sequence(x, y) ->
	     (match evalAst x variables with
		(Null, vars) -> evalAst y vars
	      | pair -> pair
	     )
	  | Arith_action arith -> (Null, snd @@ evalArith arith variables)
	  | Return arith -> evalArith arith variables
	  | Fun_defin (name, args, body) ->
	     let _ =
	       globalFunctions :=
		 EnvironmentMap.add
		   name
		   { argsNum = List.length args ; argsNames = args ; body = body }
		   !globalFunctions
	     in (Null, variables)
	  | Print arith ->
	     let pair = evalArith arith variables
	     in let _ = print_string @@ ((string_of_dataType (fst pair)) ^ " ")
		in (Null, snd pair)
	  | Println arith ->
	     let pair = evalArith arith variables
	     in let _ = print_endline @@ string_of_dataType (fst pair)
		in (Null, snd pair)
	  | Read id ->
	     (Null, snd @@ evalArith (Assign(id, Value (read_line()) )) variables)
	  | Block expr -> let pair = evalAst expr variables
			  in (fst pair, updateVariables variables @@ snd pair)
	  | If (cond, block) ->
	     let (condValue, vars) = evalArith cond variables
	     in if bool_of_value condValue then evalAst block vars
		else (Null, vars)
	  | If_else (cond, ifBlock, elseBlock) ->
	     let (condValue, vars) = evalArith cond variables
	     in if bool_of_value condValue then evalAst ifBlock vars
		else evalAst elseBlock vars
	  | While (cond, block) as whileExpr ->
	     let (condValue, vars) = evalArith cond variables
	     in if bool_of_value condValue then evalAst (Sequence(block, whileExpr)) vars
		else (Null, vars)
	)
      )
  end
;;
